#include <stdio.h>
#include<math.h>
float input_xcoor()
{
    float x;
    printf("Input X-coordinate : ");
    scanf("%f",&x);
    return x;
}
float input_ycoor()
{
    float y;
    printf("Input Y-coordinate : ");
    scanf("%f",&y);
    return y;
}
float dist(float a1, float a2, float b1, float b2)
{
    float dis;
    dis = sqrt(pow(a2 - a1, 2) + pow(b2 - b1, 2));
    printf("Distance between 2 points = %f", dis);
    return dis;
}
int main()
{
    float x1, x2, y1, y2, d;
    printf("Input the coordinates of 1st point\n");
    x1 = input_xcoor();
    y1= input_ycoor();
    printf("input the coordinates of 2nd point\n");
    x2 = input_xcoor();
    y2 = input_ycoor();
    d= dist(x1, x2, y1, y2);
    return 0;
}