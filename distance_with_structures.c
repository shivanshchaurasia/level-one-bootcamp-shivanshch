#include <stdio.h>
#include<math.h>
struct p{
    float x, y;
} a1,a2;
float input_xcoor()
{
    float x;
    printf("Input X-coordinate : ");
    scanf("%f",&x);
    return x;
}
float input_ycoor()
{
    float y;
    printf("Input Y-coordinate : ");
    scanf("%f",&y);
    return y;
}
float dist(float a1, float a2, float b1, float b2)
{
    float dis;
    dis = sqrt(pow(a2 - a1, 2) + pow(b2 - b1, 2));
    printf("Distance between 2 points = %f", dis);
    return dis;
}
int main()
{
    float d;
    printf("Input the coordinates of 1st point\n");
    a1.x = input_xcoor();
    a1.y = input_ycoor();
    printf("Input the coordinates of 2nd point\n");
    a2.x = input_xcoor();
    a2.y = input_ycoor();
    d= dist(a1.x, a2.x, a1.y, a2.y);
    return 0;
}