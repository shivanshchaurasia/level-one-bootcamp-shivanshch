//WAP to find the sum of two fractions.
#include <stdio.h>
struct frac{
    int n, d;
}; 
typedef struct frac fraction;
fraction input()
{   
    fraction f;
    scanf("%d/%d",&f.n,&f.d);
    return f; 
}    
fraction add(fraction f1, fraction f2)
{
    fraction f3;
    f3.n=(f1.n*f2.d)+(f1.d*f2.n);
    f3.d=f1.d*f2.d;
    return f3;
}
int gcd(int x, int y)
{   
    int a;
    for(int i=1; i <= x && i <= y; ++i)
    {
        if(x%i==0 && y%i==0)
        a = i;
    }
    return a;
}
void output(fraction f1, fraction f2, int m, int n, int o)
{
    printf("Sum of %d/%d + %d/%d = %d/%d", f1.n, f1.d, f2.n, f2.d, m/o,n/o);
}
int main(void)
{
    int nr, dr, g;
    fraction f1, f2, f3;
    printf("Input 1st fraction in the form of a/b : ");
    f1 = input();
    printf("Input 2nd fraction in the form of a/b : ");
    f2 = input();
    f3 = add(f1,f2);
    nr = f3.n;
    dr = f3.d;
    g = gcd(nr,dr);
    output(f1,f2,nr,dr,g);
    return 0;
}