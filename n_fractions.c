//WAP to find the sum of n fractions.
#include<stdio.h>
struct p{
   int nr,dr,gcd,tempN,tempD,ton,tod,num; 
}a;

int add()
{
    a.tempN = a.ton;
    a.tempD = a.tod;
    a.ton = ((a.tempN*a.dr)+(a.tempD*a.nr));
    a.tod = a.tempD*a.dr;
    
    for(int i = 1;i<=a.ton && i<=a.tod;i++)
    {
        if(a.ton%i == 0 && a.tod%i == 0)
        {
            a.gcd = i;
        }
    }
}

int input_nos()
{
    printf("Input the no. of fractions you want to add : ");
    scanf("%d",&a.num);
    for(int j = 1; j<=a.num;j++)
    {
        printf("Input the numerator of %d Fraction : ",j);
        scanf("%d",&a.nr);
        printf("Input the denominator of %d Fraction : ",j);
        scanf("%d",&a.dr);
        if(j == 1)
        {
            a.ton = a.nr;
            a.tod = a.dr;
        }
        else
        {
            add();
        }
    }
}
int main()
{
    input_nos();
    printf("Sum of total input fraction is : %d / %d",a.ton/a.gcd,a.tod/a.gcd);
    return 0;
}